﻿namespace Juego_del_gato
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nueve = new System.Windows.Forms.Button();
            this.ocho = new System.Windows.Forms.Button();
            this.siete = new System.Windows.Forms.Button();
            this.seis = new System.Windows.Forms.Button();
            this.cinco = new System.Windows.Forms.Button();
            this.cuatro = new System.Windows.Forms.Button();
            this.tres = new System.Windows.Forms.Button();
            this.dos = new System.Windows.Forms.Button();
            this.uno = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_CvsC = new System.Windows.Forms.Button();
            this.btn_JvsC = new System.Windows.Forms.Button();
            this.btn_JvsJ = new System.Windows.Forms.Button();
            this.radioJugador2 = new System.Windows.Forms.RadioButton();
            this.button3 = new System.Windows.Forms.Button();
            this.radioJugador1 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cpuFirst = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label_posiciones = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.nueve);
            this.groupBox1.Controls.Add(this.ocho);
            this.groupBox1.Controls.Add(this.siete);
            this.groupBox1.Controls.Add(this.seis);
            this.groupBox1.Controls.Add(this.cinco);
            this.groupBox1.Controls.Add(this.cuatro);
            this.groupBox1.Controls.Add(this.tres);
            this.groupBox1.Controls.Add(this.dos);
            this.groupBox1.Controls.Add(this.uno);
            this.groupBox1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 273);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Area de juego";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "          7                          8                       9";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(203, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "          4                           5                      6";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "          1                           2                       3 ";
            // 
            // nueve
            // 
            this.nueve.Enabled = false;
            this.nueve.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nueve.Location = new System.Drawing.Point(175, 198);
            this.nueve.Name = "nueve";
            this.nueve.Size = new System.Drawing.Size(75, 60);
            this.nueve.TabIndex = 8;
            this.nueve.UseVisualStyleBackColor = true;
            this.nueve.Click += new System.EventHandler(this.sendL);
            // 
            // ocho
            // 
            this.ocho.Enabled = false;
            this.ocho.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ocho.Location = new System.Drawing.Point(95, 198);
            this.ocho.Name = "ocho";
            this.ocho.Size = new System.Drawing.Size(75, 60);
            this.ocho.TabIndex = 7;
            this.ocho.UseVisualStyleBackColor = true;
            this.ocho.Click += new System.EventHandler(this.sendL);
            // 
            // siete
            // 
            this.siete.Enabled = false;
            this.siete.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siete.Location = new System.Drawing.Point(13, 198);
            this.siete.Name = "siete";
            this.siete.Size = new System.Drawing.Size(75, 60);
            this.siete.TabIndex = 6;
            this.siete.UseVisualStyleBackColor = true;
            this.siete.Click += new System.EventHandler(this.sendL);
            // 
            // seis
            // 
            this.seis.Enabled = false;
            this.seis.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seis.Location = new System.Drawing.Point(175, 118);
            this.seis.Name = "seis";
            this.seis.Size = new System.Drawing.Size(75, 57);
            this.seis.TabIndex = 5;
            this.seis.UseVisualStyleBackColor = true;
            this.seis.Click += new System.EventHandler(this.sendL);
            // 
            // cinco
            // 
            this.cinco.Enabled = false;
            this.cinco.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cinco.Location = new System.Drawing.Point(95, 118);
            this.cinco.Name = "cinco";
            this.cinco.Size = new System.Drawing.Size(74, 58);
            this.cinco.TabIndex = 4;
            this.cinco.UseVisualStyleBackColor = true;
            this.cinco.Click += new System.EventHandler(this.sendL);
            // 
            // cuatro
            // 
            this.cuatro.Enabled = false;
            this.cuatro.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuatro.Location = new System.Drawing.Point(13, 118);
            this.cuatro.Name = "cuatro";
            this.cuatro.Size = new System.Drawing.Size(75, 58);
            this.cuatro.TabIndex = 3;
            this.cuatro.UseVisualStyleBackColor = true;
            this.cuatro.Click += new System.EventHandler(this.sendL);
            // 
            // tres
            // 
            this.tres.Enabled = false;
            this.tres.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tres.Location = new System.Drawing.Point(175, 37);
            this.tres.Name = "tres";
            this.tres.Size = new System.Drawing.Size(75, 59);
            this.tres.TabIndex = 2;
            this.tres.UseVisualStyleBackColor = true;
            this.tres.Click += new System.EventHandler(this.sendL);
            // 
            // dos
            // 
            this.dos.Enabled = false;
            this.dos.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dos.Location = new System.Drawing.Point(95, 37);
            this.dos.Name = "dos";
            this.dos.Size = new System.Drawing.Size(74, 59);
            this.dos.TabIndex = 1;
            this.dos.UseVisualStyleBackColor = true;
            this.dos.Click += new System.EventHandler(this.sendL);
            // 
            // uno
            // 
            this.uno.Enabled = false;
            this.uno.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uno.Location = new System.Drawing.Point(13, 37);
            this.uno.Name = "uno";
            this.uno.Size = new System.Drawing.Size(75, 60);
            this.uno.TabIndex = 0;
            this.uno.UseVisualStyleBackColor = true;
            this.uno.Click += new System.EventHandler(this.sendL);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_CvsC);
            this.groupBox2.Controls.Add(this.btn_JvsC);
            this.groupBox2.Controls.Add(this.btn_JvsJ);
            this.groupBox2.Controls.Add(this.radioJugador2);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.radioJugador1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(292, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(235, 273);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opciones";
            // 
            // btn_CvsC
            // 
            this.btn_CvsC.Location = new System.Drawing.Point(9, 152);
            this.btn_CvsC.Name = "btn_CvsC";
            this.btn_CvsC.Size = new System.Drawing.Size(220, 35);
            this.btn_CvsC.TabIndex = 10;
            this.btn_CvsC.Text = "CPU vs CPU";
            this.btn_CvsC.UseVisualStyleBackColor = true;
            this.btn_CvsC.Click += new System.EventHandler(this.btn_CvsC_Click);
            // 
            // btn_JvsC
            // 
            this.btn_JvsC.Location = new System.Drawing.Point(9, 111);
            this.btn_JvsC.Name = "btn_JvsC";
            this.btn_JvsC.Size = new System.Drawing.Size(220, 35);
            this.btn_JvsC.TabIndex = 9;
            this.btn_JvsC.Text = "Jugador vs CPU\r\n";
            this.btn_JvsC.UseVisualStyleBackColor = true;
            this.btn_JvsC.Click += new System.EventHandler(this.btn_JvsC_Click);
            // 
            // btn_JvsJ
            // 
            this.btn_JvsJ.Location = new System.Drawing.Point(9, 70);
            this.btn_JvsJ.Name = "btn_JvsJ";
            this.btn_JvsJ.Size = new System.Drawing.Size(220, 35);
            this.btn_JvsJ.TabIndex = 8;
            this.btn_JvsJ.Text = "Jugador vs Jugador";
            this.btn_JvsJ.UseVisualStyleBackColor = true;
            this.btn_JvsJ.Click += new System.EventHandler(this.btn_JvsJ_Click);
            // 
            // radioJugador2
            // 
            this.radioJugador2.AutoSize = true;
            this.radioJugador2.Enabled = false;
            this.radioJugador2.Location = new System.Drawing.Point(133, 37);
            this.radioJugador2.Name = "radioJugador2";
            this.radioJugador2.Size = new System.Drawing.Size(14, 13);
            this.radioJugador2.TabIndex = 3;
            this.radioJugador2.TabStop = true;
            this.radioJugador2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(9, 198);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(220, 41);
            this.button3.TabIndex = 7;
            this.button3.Text = "Salir";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // radioJugador1
            // 
            this.radioJugador1.AutoSize = true;
            this.radioJugador1.Enabled = false;
            this.radioJugador1.Location = new System.Drawing.Point(30, 37);
            this.radioJugador1.Name = "radioJugador1";
            this.radioJugador1.Size = new System.Drawing.Size(14, 13);
            this.radioJugador1.TabIndex = 2;
            this.radioJugador1.TabStop = true;
            this.radioJugador1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "X=";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "O=";
            // 
            // cpuFirst
            // 
            this.cpuFirst.AutoSize = true;
            this.cpuFirst.Location = new System.Drawing.Point(533, 194);
            this.cpuFirst.Name = "cpuFirst";
            this.cpuFirst.Size = new System.Drawing.Size(94, 17);
            this.cpuFirst.TabIndex = 3;
            this.cpuFirst.Text = "CPU 1 primero";
            this.cpuFirst.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label_posiciones);
            this.groupBox3.Location = new System.Drawing.Point(533, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(247, 175);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Posiciones dispoinbles";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(229, 78);
            this.label7.TabIndex = 1;
            this.label7.Text = "Las posiciones que estan en \r\n0 son las no disponibles.\r\n\r\nEn CPU vs CPU, \r\nX es " +
    "la IA con Machine Learning CPU 1,\r\nO es la IA con estrategia preestablecida CPU " +
    "2";
            // 
            // label_posiciones
            // 
            this.label_posiciones.AutoSize = true;
            this.label_posiciones.Location = new System.Drawing.Point(19, 42);
            this.label_posiciones.Name = "label_posiciones";
            this.label_posiciones.Size = new System.Drawing.Size(0, 13);
            this.label_posiciones.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 292);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.cpuFirst);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button tres;
        private System.Windows.Forms.Button dos;
        private System.Windows.Forms.Button uno;
        private System.Windows.Forms.Button nueve;
        private System.Windows.Forms.Button ocho;
        private System.Windows.Forms.Button siete;
        private System.Windows.Forms.Button seis;
        private System.Windows.Forms.Button cinco;
        private System.Windows.Forms.Button cuatro;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label_posiciones;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioJugador2;
        private System.Windows.Forms.RadioButton radioJugador1;
        private System.Windows.Forms.CheckBox cpuFirst;
        private System.Windows.Forms.Button btn_CvsC;
        private System.Windows.Forms.Button btn_JvsC;
        private System.Windows.Forms.Button btn_JvsJ;
    }
}

