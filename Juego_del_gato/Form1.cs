﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Juego_del_gato
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        bool x = false;
        bool o = true;
        bool gameFinished = false;
        string equis = "";
        string circulo = "";
        string connectionString = "Server=localhost; Port=3306; User id=root; Database=Gato; Password=";
        // maxUses es el maximo de usos que se le da a una respuesta para probarla
        int maxUses = 5;
        // { {idPatron,resp}, ...  }
        int[,] xAnswers = new int[5, 2];
        int[,] oAnswers = new int[5, 2];
        int caso = 0;
        public void GanarPartida()
        {
            //Horizontal
            if (uno.Text.Equals("O") && dos.Text.Equals("O") && tres.Text.Equals("O"))
            {
                MessageBox.Show("ha ganado  " + circulo);
                updateAnswerWins("O");
                limpiar();
            }
            else if (cuatro.Text.Equals("O") && cinco.Text.Equals("O") && seis.Text.Equals("O"))
            {
                MessageBox.Show("ha ganado  " + circulo);
                updateAnswerWins("O");
                limpiar();
            }
            else if (siete.Text.Equals("O") && ocho.Text.Equals("O") && nueve.Text.Equals("O"))
            {
                MessageBox.Show("ha ganado  " + circulo);
                updateAnswerWins("O");
                limpiar();
            }
            //vertical
            else if (uno.Text.Equals("O") && cuatro.Text.Equals("O") && siete.Text.Equals("O"))
            {
                MessageBox.Show("ha ganado  " + circulo);
                updateAnswerWins("O");
                limpiar();
            }
            else if (dos.Text.Equals("O") && cinco.Text.Equals("O") && ocho.Text.Equals("O"))
            {
                MessageBox.Show("ha ganado  " + circulo);
                updateAnswerWins("O");
                limpiar();
            }
            else if (tres.Text.Equals("O") && seis.Text.Equals("O") && nueve.Text.Equals("O"))
            {
                MessageBox.Show("ha ganado  " + circulo);
                updateAnswerWins("O");
                limpiar();
            }
            //diagonal
            else if (uno.Text.Equals("O") && cinco.Text.Equals("O") && nueve.Text.Equals("O"))
            {
                MessageBox.Show("ha ganado  " + circulo);
                updateAnswerWins("O");
                limpiar();
            }
            else if (tres.Text.Equals("O") && cinco.Text.Equals("O") && siete.Text.Equals("O"))
            {
                MessageBox.Show("ha ganado  " + circulo);
                updateAnswerWins("O");
                limpiar();
            }
            //  X Horizontal
            else if (uno.Text.Equals("X") && dos.Text.Equals("X") && tres.Text.Equals("X"))
            {
                MessageBox.Show("ha ganado  " + equis);
                updateAnswerWins("X");
                limpiar();
            }
            else if (cuatro.Text.Equals("X") && cinco.Text.Equals("X") && seis.Text.Equals("X"))
            {
                MessageBox.Show("ha ganado  " + equis);
                updateAnswerWins("X");
                limpiar();
            }
            else if (siete.Text.Equals("X") && ocho.Text.Equals("X") && nueve.Text.Equals("X"))
            {
                MessageBox.Show("ha ganado  " + equis);
                updateAnswerWins("X");
                limpiar();
            }
            //vertical
            else if (uno.Text.Equals("X") && cuatro.Text.Equals("X") && siete.Text.Equals("X"))
            {
                MessageBox.Show("ha ganado  " + equis);
                updateAnswerWins("X");
                limpiar();
            }
            else if (dos.Text.Equals("X") && cinco.Text.Equals("X") && ocho.Text.Equals("X"))
            {
                MessageBox.Show("ha ganado  " + equis);
                updateAnswerWins("X");
                limpiar();
            }
            else if (tres.Text.Equals("X") && seis.Text.Equals("X") && nueve.Text.Equals("X"))
            {
                MessageBox.Show("ha ganado  " + equis);
                updateAnswerWins("X");
                limpiar();
            }
            //diagonal
            else if (uno.Text.Equals("X") && cinco.Text.Equals("X") && nueve.Text.Equals("X"))
            {
                MessageBox.Show("ha ganado  " + equis);
                updateAnswerWins("X");
                limpiar();
            }
            else if (tres.Text.Equals("X") && cinco.Text.Equals("X") && siete.Text.Equals("X"))
            {
                MessageBox.Show("ha ganado  " + equis);
                updateAnswerWins("X");
                limpiar();
            }
            else
            {
                empate();
            }
        }
        //metodo limpiar 
        public void limpiar()
        {
            List<Button> celdas = new List<Button> { uno, dos, tres, cuatro, cinco, seis, siete, ocho, nueve };
            foreach (var celda in celdas)
            {
                celda.Enabled = false;
                celda.Text = "";
            }
            x = false;
            o = true;
            xAnswers = new int[5, 2];
            oAnswers = new int[5, 2];
            caso = 0;
            cpuFirst.Enabled = true;
        }
        //Metodo Empate
        public void empate()
        {
            if (!((uno.Enabled) || (dos.Enabled) || (tres.Enabled) || (cuatro.Enabled) || (cinco.Enabled) || (seis.Enabled)
                 || (siete.Enabled) || (ocho.Enabled) || (nueve.Enabled)))
            {
                MessageBox.Show("EMPATE");
                // se considera como gane a un empate para que la IA aprenda a forzar empates
                updateAnswerWins("O");
                updateAnswerWins("X");
                limpiar();
            }
        }
        //Metodo iniciarPartida
        public void IniciarJuego()
        {
            gameFinished = false;
            if (caso == 2)
            {
                equis = "Jugador 1";
                circulo = "Jugador 2";
                radioJugador1.Text = circulo;
                radioJugador2.Text = equis;
                label2.Visible = true;
                DibujarTablero();
            }
            else
            {
                equis = !cpuFirst.Checked ? "CPU" : "Jugador";
                circulo = cpuFirst.Checked ? "CPU" : "Jugador";
                radioJugador1.Text = circulo;
                radioJugador2.Text = equis;
                label2.Visible = true;
                DibujarTablero();
                radioJugador1.Checked = true;
                cpuFirst.Enabled = false;
                if (cpuFirst.Checked)
                {
                    x = false;
                    o = true;
                    System.Threading.Thread.Sleep(1000);
                    ganarAprender("O",true,0);
                    o = false;
                    x = true;
                    radioJugador2.Checked = true;
                }
                
            }

        }
        //Metodo DibujarTablero
        public void DibujarTablero()
        {
            uno.Enabled = true;
            dos.Enabled = true;
            tres.Enabled = true;
            cuatro.Enabled = true;
            cinco.Enabled = true;
            seis.Enabled = true;
            siete.Enabled = true;
            ocho.Enabled = true;
            nueve.Enabled = true;
        }
        //Metodo para calcular casillas libres
        public int[] BuscarCasilla()
        {
            int[] casillas = { 1, 2, 3, 4, 5, 6, 7, 8, 9};
            if (uno.Enabled == false)
            {
                casillas[0] = 0;
            }
            if (dos.Enabled == false)
            {
                casillas[1] = 0;
            }
            if (tres.Enabled == false)
            {
                casillas[2] = 0;
            }
           if (cuatro.Enabled == false)
            {
                casillas[3] = 0;
            }
            if (cinco.Enabled == false)
            {
                casillas[4] = 0;
            }
            if (seis.Enabled == false)
            {
                casillas[5] = 0;
            }
            if (siete.Enabled == false)
            {
                casillas[6] = 0;
            }
           if (ocho.Enabled == false)
            {
                casillas[7] = 0;
            }
            if (nueve.Enabled == false)
            {
                casillas[8] = 0;
            }
            string cad = "";
            for (int i = 0; i < casillas.Length; i++)
            {
                //  label_posiciones.Text=posiciones[i] + "\t");
                cad = cad + casillas[i] + "   ";
                label_posiciones.Text = cad;
            }
            return casillas;

        }
        //Metodo para asignar figuras
        private void sendL(object sender, System.EventArgs e)
        {
            Button btn = (Button)sender;
            // Console.WriteLine(getCasilla(btn.Name));
            switch (caso)
            {
                case 1:
                    if (cpuFirst.Checked)
                    {
                        if (x == true)
                        {
                            x = false;
                            o = true;
                            ganarAprender("X",false,getCasilla(btn.Name));
                            btn.Text = "X";
                            btn.Enabled = false;
                            BuscarCasilla();
                            radioJugador1.Checked = true;
                            GanarPartida();
                            if (!gameFinished)
                            {
                                System.Threading.Thread.Sleep(1000);
                                ganarAprender("O", true, 0);
                                o = false;
                                x = true;
                                radioJugador2.Checked = true;
                                BuscarCasilla();
                                GanarPartida();
                            }
                        }
                    }
                    else
                    {
                        if (o == true)
                        {
                            o = false;
                            x = true;
                            ganarAprender("O", false, getCasilla(btn.Name));
                            btn.Text = "O";
                            btn.Enabled = false;
                            BuscarCasilla();
                            radioJugador2.Checked = true;
                            GanarPartida();
                            if (!gameFinished)
                            {
                                System.Threading.Thread.Sleep(1000);
                                ganarAprender("X", true, 0);
                                x = false;
                                o = true;
                                radioJugador1.Checked = true;
                                BuscarCasilla();
                                GanarPartida();
                            }
                        }
                    }
                    
                    break;
                case 2:
                    if (x == true)
                    {
                        btn.Text = "X";
                        x = false;
                        o = true;
                        btn.Enabled = false;
                        BuscarCasilla();
                        radioJugador2.Checked = true;
                        GanarPartida();
                    }
                    else
                    {
                        btn.Text = "O";
                        x = true;
                        o = false;
                        btn.Enabled = false;
                        BuscarCasilla();
                        radioJugador1.Checked = true;
                        GanarPartida();
                    }
                    break;
            }

            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        public int formarLinea(int[] celdas, string Simbolo, int opc)
        {
            int curOpc = 1;
            int c = 0;
            if ((Array.Exists(celdas, n => n == 2) && Array.Exists(celdas, n => n == 3)) || (Array.Exists(celdas, n => n == 4) && Array.Exists(celdas, n => n == 7)) || (Array.Exists(celdas, n => n == 5) && Array.Exists(celdas, n => n == 9)))
            {
                c = opc == curOpc ? 1 : c;
                curOpc++;
            }
            if ((Array.Exists(celdas, n => n == 1) && Array.Exists(celdas, n => n == 3)) || (Array.Exists(celdas, n => n == 5) && Array.Exists(celdas, n => n == 8)))
            {
                c = opc == curOpc ? 2 : c;
                curOpc++;
            }
            if ((Array.Exists(celdas, n => n == 1) && Array.Exists(celdas, n => n == 2)) || (Array.Exists(celdas, n => n == 7) && Array.Exists(celdas, n => n == 5)) || (Array.Exists(celdas, n => n == 6) && Array.Exists(celdas, n => n == 9)))
            {
                c = opc == curOpc ? 3 : c;
                curOpc++;
            }
            if ((Array.Exists(celdas, n => n == 1) && Array.Exists(celdas, n => n == 7)) || (Array.Exists(celdas, n => n == 5) && Array.Exists(celdas, n => n == 6)))
            {
                c = opc == curOpc ? 4 : c;
                curOpc++;
            }
            if ((Array.Exists(celdas, n => n == 1) && Array.Exists(celdas, n => n == 9)) || (Array.Exists(celdas, n => n == 2) && Array.Exists(celdas, n => n == 8)) || (Array.Exists(celdas, n => n == 3) && Array.Exists(celdas, n => n == 7)) || (Array.Exists(celdas, n => n == 4) && Array.Exists(celdas, n => n == 6)))
            {
                c = opc == curOpc ? 5 : c;
                curOpc++;
            }
            if ((Array.Exists(celdas, n => n == 4) && Array.Exists(celdas, n => n == 5)) || (Array.Exists(celdas, n => n == 3) && Array.Exists(celdas, n => n == 9)))
            {
                c = opc == curOpc ? 6 : c;
                curOpc++;
            }
            if ((Array.Exists(celdas, n => n == 1) && Array.Exists(celdas, n => n == 4)) || (Array.Exists(celdas, n => n == 3) && Array.Exists(celdas, n => n == 5)) || (Array.Exists(celdas, n => n == 8) && Array.Exists(celdas, n => n == 9)))
            {
                c = opc == curOpc ? 7 : c;
                curOpc++;
            }
            if ((Array.Exists(celdas, n => n == 2) && Array.Exists(celdas, n => n == 5)) || (Array.Exists(celdas, n => n == 7) && Array.Exists(celdas, n => n == 9)))
            {
                c = opc == curOpc ? 8 : c;
                curOpc++;
            }
            if ((Array.Exists(celdas, n => n == 7) && Array.Exists(celdas, n => n == 8)) || (Array.Exists(celdas, n => n == 1) && Array.Exists(celdas, n => n == 5)) || (Array.Exists(celdas, n => n == 3) && Array.Exists(celdas, n => n == 6)))
            {
                c = opc == curOpc ? 9 : c;
                curOpc++;
            }
            return c;
        }
        
        void updateAnswerWins(string simbol)
        {
            gameFinished = true;
            switch (simbol)
            {
                case "X":
                    for (int i = 0; i < 5; i++)
                    {
                        if (xAnswers[i, 1] != 0)
                        {
                            int[] data = getDataAnswer(xAnswers[i, 0], xAnswers[i, 1]);
                            updateAnswer(xAnswers[i,0], xAnswers[i,1], data[1], data[0] + 1);
                        }
                    }
                    break;
                case "O":
                    for (int i = 0; i < 5; i++)
                    {
                        if (oAnswers[i, 1] != 0)
                        {
                            int[] data = getDataAnswer(oAnswers[i, 0], oAnswers[i, 1]);
                            updateAnswer(oAnswers[i, 0], oAnswers[i, 1], data[1], data[0] + 1);
                        }
                    }
                    break;
            }
        }

        object[] patternExists(string pattern, int turn)
        {
            // existe?, id, rotadas
            object[] p = new object[3];
            p[0] = false;
            string[] rotatedpatterns = getRotatedPatterns(pattern);
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    // si el patron existe verificamos que tenga respuestas y de ser asi elegimos la mas apropiada
                    string Query = "SELECT id, patron FROM Jugada WHERE turno=" + turn;

                    using (MySqlCommand Command = new MySqlCommand(Query, connection))
                    {
                        using (MySqlDataReader reader = Command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                pattern = reader.GetString("patron");
                                for (int i = 0; i < 4; i++)
                                {
                                    if (pattern == rotatedpatterns[i])
                                    {
                                        p[0] = true;
                                        p[1] = reader.GetInt32("id");
                                        p[2] = i;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("error patternExists: " + ex.ToString());
            }
            return p;
        }

        void setPattern(string pattern, int turn)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    //en caso de que el patron no exista se inserta en la base
                    string Query = "INSERT INTO Jugada (patron,turno) values('" + pattern + "'," + turn + ")";
                    using (MySqlCommand Command = new MySqlCommand(Query, connection))
                    {
                        Command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("error setPattern: " + ex.ToString());
            }
        }

        string getPattern(int[] p1, int[] p0)
        {
            string pattern = "";
            char[] Arr = {'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n' };

            foreach (var c in p1)
            {
                if (c != 0)
                {
                    Arr[c - 1] = '1';
                }
            }
            foreach (var c in p0)
            {
                if (c != 0)
                {
                    Arr[c - 1] = '0';
                }
            }
            pattern = new string(Arr);
            return pattern;
        }

        int getRotatedAnswerLeft(int answer, int rotated)
        {
            switch (rotated)
            {
                case 1:
                    return answer == 1 ? 7 : answer == 2 ? 4 : answer == 3 ? 1 : answer == 4 ? 8 : answer == 6 ? 2 : answer == 7 ? 9 : answer == 8 ? 6 : answer == 9 ? 3 : answer;
                case 2:
                    return answer == 1 ? 9 : answer == 2 ? 8 : answer == 3 ? 7 : answer == 4 ? 6 : answer == 6 ? 4 : answer == 7 ? 3 : answer == 8 ? 2 : answer == 9 ? 1 : answer;
                case 3:
                    return answer == 1 ? 3 : answer == 2 ? 6 : answer == 3 ? 9 : answer == 4 ? 2 : answer == 6 ? 8 : answer == 7 ? 1 : answer == 8 ? 4 : answer == 9 ? 7 : answer;
                default:
                    return answer;
            }
        }

        int getRotatedAnswerRight(int answer, int rotated)
        {
            switch (rotated)
            {
                case 1:
                    return answer == 1 ? 3 : answer == 2 ? 6 : answer == 3 ? 9 : answer == 4 ? 2 : answer == 6 ? 8 : answer == 7 ? 1 : answer == 8 ? 4 : answer == 9 ? 7 : answer;
                case 2:
                    return answer == 1 ? 9 : answer == 2 ? 8 : answer == 3 ? 7 : answer == 4 ? 6 : answer == 6 ? 4 : answer == 7 ? 3 : answer == 8 ? 2 : answer == 9 ? 1 : answer;
                case 3:
                    return answer == 1 ? 7 : answer == 2 ? 4 : answer == 3 ? 1 : answer == 4 ? 8 : answer == 6 ? 2 : answer == 7 ? 9 : answer == 8 ? 6 : answer == 9 ? 3 : answer;
                default:
                    return answer;
            }
        }

        string[] getRotatedPatterns(string pattern)
        {
            string[] patterns = new string[4];
            char[] Arr = pattern.ToCharArray();
            patterns[0] = pattern;
            patterns[1] = Arr[8].ToString() + Arr[7].ToString() + Arr[6].ToString() + Arr[5].ToString() + Arr[4].ToString() + Arr[3].ToString() + Arr[2].ToString() + Arr[1].ToString() + Arr[0].ToString();
            patterns[2] = Arr[2].ToString() + Arr[5].ToString() + Arr[8].ToString() + Arr[1].ToString() + Arr[4].ToString() + Arr[7].ToString() + Arr[0].ToString() + Arr[3].ToString() + Arr[6].ToString();
            patterns[3] = Arr[6].ToString() + Arr[3].ToString() + Arr[0].ToString() + Arr[7].ToString() + Arr[4].ToString() + Arr[1].ToString() + Arr[8].ToString() + Arr[5].ToString() + Arr[2].ToString();
            return patterns;
        }

        int getRandomAnswer()
        {
            Random r = new Random();
            int answer = 0;
            do
            {
                answer = r.Next(8) + 1;
            } while (!BuscarCasilla().Contains(answer));
            return answer;
        }

        void setAnswer(int id, int answer)
        {
            if (answer != 0)
            {
                try
                {
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                    {
                        connection.Open();
                        string Query = "INSERT INTO Jugada_Respuesta (Jugada_id,resp) values(" + id + ",'" + answer + "')";
                        using (MySqlCommand Command = new MySqlCommand(Query, connection))
                        {
                            Command.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("error setAnswer: " + ex.ToString());
                }
            }
        }

        void updateAnswer(int id, int answer, int uses, int wins)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    string Query = "UPDATE Jugada_Respuesta SET gane=" + wins + " ,usos=" + uses + " WHERE resp = " + answer + " AND Jugada_id =" + id;
                    using (MySqlCommand Command = new MySqlCommand(Query, connection))
                    {
                        Command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception err)
            {
                Console.WriteLine("updateAnswer error" + err);
            }
        }

        int[] getDataAnswer(int id, int answer)
        {
            // data contiene gane y usos
            int[] data = new int[2];
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();
                // si el patron existe verificamos que tenga respuestas y de ser asi elegimos la mas apropiada
                string Query = "SELECT gane, usos FROM Jugada_Respuesta WHERE Jugada_id=" + id + " AND resp=" + answer;

                using (MySqlCommand Command = new MySqlCommand(Query, connection))
                {
                    using (MySqlDataReader reader = Command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            data[0] = reader.GetInt32("gane");
                            data[1] = reader.GetInt32("usos");
                        }
                    }
                }
            }
            return data;
        }

        // rotated es la veces que fue rotado para obtener la respuesta adecuada
        int getAnswer(int id, int rotated)
        {
            int answer = 0;
            int[] allAnswers = new int[9];
            int[,] answersData = new int[9,3];
            // uses es el limite de usos que se le da a una respuesta, entre menor sea descarta una respuesta mas rapido
            int uses = maxUses, wins = 0, a = 0;
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    // si el patron existe verificamos que tenga respuestas y de ser asi elegimos la mas apropiada
                    string Query = "SELECT resp, gane, usos FROM Jugada_Respuesta WHERE Jugada_id=" + id;

                    using (MySqlCommand Command = new MySqlCommand(Query, connection))
                    {
                        using (MySqlDataReader reader = Command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int an = reader.GetInt32("resp");
                                int wi = reader.GetInt32("gane");
                                int us = reader.GetInt32("usos");

                                allAnswers[a] = an;
                                if (us <= uses)
                                {
                                    if (us < uses)
                                    {
                                        answer = an;
                                        wins = wi;
                                        uses = us;
                                    }
                                    else if (wi >= wins)
                                    {
                                        answer = an;
                                        wins = wi;
                                        uses = us;
                                    }
                                }
                                a++;
                            }
                        }
                    }
                }

                // en caso de no haya respuestas
                if (answer == 0)
                {
                    answer = getRandomAnswer();
                    // se inserta la nueva respuesta en la base
                    setAnswer(id, answer);
                }
                else
                {
                    // se actualiza la respuesta en base
                    uses = uses < maxUses ? uses + 1 : uses;
                    updateAnswer(id, answer, uses, wins);
                    answer = getRotatedAnswerRight(answer, rotated);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("error getAnswer: " + ex.ToString());
            }
            return answer;
        }

        void Answer(int id, int rotated, int answer)
        {
            /// rotar antes la respuesta inv 3 --> 7
            int ans = getRotatedAnswerLeft(answer, rotated);
            int uses = 0, wins = 0;
            bool ansExists = false;
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    string Query = "SELECT gane, usos FROM Jugada_Respuesta WHERE Jugada_id=" + id + " AND resp=" + ans;

                    using (MySqlCommand Command = new MySqlCommand(Query, connection))
                    {
                        using (MySqlDataReader reader = Command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ansExists = true;
                                wins = reader.GetInt32("gane");
                                uses = reader.GetInt32("usos");
                            }
                        }
                    }
                }
                // en caso de que la respuesta exista
                if (ansExists)
                {
                    uses = uses < maxUses ? uses + 1 : uses;
                    updateAnswer(id, ans, uses, wins);
                }
                else
                {
                    // se inserta la nueva respuesta en la base
                    setAnswer(id, ans);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("error Answer: " + ex.ToString());
            }
        }

        public void ganarAprender(string simbol, bool mark, int answer)
        {
            
            int[] equis = new int[5]; // arreglo de celdas con X
            int[] circulos = new int[5]; // arreglo de celdas con O
            Random r = new Random();
            int c = 0, e = 0; // indices

            // arreglo con todos los botones (celdas) en el juego
            List<Button> celdas = new List<Button> { uno, dos, tres, cuatro, cinco, seis, siete, ocho, nueve };

            for (int i = 0; i < 9; i++)
            {
                if (celdas[i].Enabled == false)
                {
                    if (celdas[i].Text.Equals("X"))
                    {
                        equis[e] = i + 1;
                        e++;
                    }
                    else 
                    if (celdas[i].Text.Equals("O"))
                    {
                        circulos[c] = i + 1;
                        c++;
                    }
                }
            }
            
            o = simbol.Equals("X");
            x = simbol.Equals("O");

            int[] P1 = (c + e) % 2 == 0 ? equis : circulos; // arreglo de celdas de P1 (jugador en turno)
            int[] P2 = (c + e) % 2 == 0 ? circulos : equis; // arreglo de celdas de P2

            // c + e indica el turno
            string pattern = getPattern(P1, P2);
            int rotated = 0;
            int Jugada_id = 0;

            object[] ptrn = patternExists(pattern, c + e);
            //si el patron existe
            if (ptrn[0].Equals(true))
            {
                Jugada_id = Convert.ToInt32(ptrn[1]);
                rotated = Convert.ToInt32(ptrn[2]);

                if (mark)
                {
                    answer = getAnswer(Jugada_id, rotated);
                    if (answer != 0)
                    {
                        marcarCasilla(answer, simbol);
                    }
                }
                else
                {
                    Answer(Jugada_id, rotated, answer);
                }
                
            }
            else
            {
                if (mark)
                {
                    setPattern(getPattern(P1, P2), c + e);
                    object[] ptrn2 = patternExists(getPattern(P1, P2), c + e);
                    Jugada_id = Convert.ToInt32(ptrn2[1]);
                    rotated = Convert.ToInt32(ptrn2[2]);
                    answer = getAnswer(Jugada_id , rotated);
                    if (answer != 0)
                    {
                        marcarCasilla(answer, simbol);
                    }
                }
                else
                {
                    setPattern(pattern, c + e);
                    Jugada_id = Convert.ToInt32(patternExists(pattern, c + e)[1]);
                    rotated = Convert.ToInt32(patternExists(pattern, c + e)[2]);
                    setAnswer(Jugada_id, answer);
                }
            }

            xAnswers[e, 0] = Jugada_id;
            xAnswers[e, 1] = getRotatedAnswerRight(answer, rotated);

            oAnswers[c, 0] = Jugada_id;
            oAnswers[c, 1] = getRotatedAnswerRight(answer, rotated);

        }
        public int ganarEstrategia(string simboloCPU)
        {
            int answer = 0;
            int[] equis = new int[5]; // arreglo de celdas con X
            int[] circulos = new int[5]; // arreglo de celdas con O
            Random r = new Random();
            int c = 0, e = 0; // indices

            // arreglo con todos los botones (celdas) en el juego
            List<Button> celdas = new List<Button> { uno, dos, tres, cuatro, cinco, seis, siete, ocho, nueve };

            for (int i = 0; i < 9; i++)
            {
                if (celdas[i].Enabled == false)
                {
                    if (celdas[i].Text.Equals("X"))
                    {
                        equis[e] = i + 1;
                        e++;
                    }
                    else
                    if (celdas[i].Text.Equals("O"))
                    {
                        circulos[c] = i + 1;
                        c++;
                    }
                }
            }

            o = simboloCPU.Equals("X");
            x = simboloCPU.Equals("O");

            int[] CPU = simboloCPU.Equals("X") ? equis : circulos; // arreglo de celdas del CPU
            int[] User = simboloCPU.Equals("O") ? equis : circulos; // arreglo de celdas del Usuario
            int cs = 0; //casilla seleccionada
            
            switch (c + e)
            {
                case 0: // CPU primero
                    switch (r.Next(3))
                    {
                        case 0:
                            answer = 1;
                            break;
                        case 1:
                            answer = 3;
                            break;
                        case 2:
                            answer = 7;
                            break;
                        case 3:
                            answer = 9;
                            break;
                    }
                    break;
                case 1:
                    if (User[0] == 5) // si usuario tiro en centro
                    {
                        switch (r.Next(3))
                        {
                            case 0:
                                answer = 1;
                                break;
                            case 1:
                                answer = 3;
                                break;
                            case 2:
                                answer = 7;
                                break;
                            case 3:
                                answer = 9;
                                break;
                        }
                    }
                    else // si usuario tiro en esquina o borde
                    {
                        answer = 5;
                    }

                    break;
                case 2:
                    if (User[0] != 5) // si el usuario no eligio centro
                    {
                        switch (CPU[0])
                        {
                            case 1: // 1
                                if (User[0] == 4 || User[0] == 7)
                                {
                                    answer = 3;
                                }
                                else
                                {
                                    answer = 7;
                                }
                                break;
                            case 3: // 3
                                if (User[0] == 6 || User[0] == 9)
                                {
                                    answer = 1;
                                }
                                else
                                {
                                    answer = 9;
                                }
                                break;
                            case 7: // 7
                                if (User[0] == 8 || User[0] == 9)
                                {
                                    answer = 1;
                                }
                                else
                                {
                                    answer = 9;
                                }
                                break;
                            case 9: // 9
                                if (User[0] == 7 || User[0] == 8)
                                {
                                    answer = 3;
                                }
                                else
                                {
                                    answer = 7;
                                }
                                break;
                        }
                    }
                    else
                    {
                        switch (CPU[0])
                        {
                            case 1:
                                answer = 9;
                                break;
                            case 3:
                                answer = 7;
                                break;
                            case 7:
                                answer = 3;
                                break;
                            case 9:
                                answer = 1;
                                break;
                        }
                    }
                    break;
                case 3: // turno 4
                    if (CPU[0] == 5)
                    {
                        if (Array.Exists(User, n => n == 1))
                        {
                            if (Array.Exists(User, n => n == 2))
                            {
                                answer = 3;
                            }
                            else
                            if (Array.Exists(User, n => n == 3))
                            {
                                answer = 2;
                            }
                            else
                            if (Array.Exists(User, n => n == 4))
                            {
                                answer = 7;
                            }
                            else
                            if (Array.Exists(User, n => n == 7))
                            {
                                answer = 4;
                            }
                            else
                            {
                                if (Array.Exists(User, n => n == 6))
                                {
                                    answer = 2;
                                }
                                else
                                {
                                    answer = 4;
                                }
                            }
                        } else
                        if (Array.Exists(User, n => n == 3))
                        {
                            if (Array.Exists(User, n => n == 2))
                            {
                                answer = 1;
                            }
                            else
                            if (Array.Exists(User, n => n == 1))
                            {
                                answer = 2;
                            }
                            else
                            if (Array.Exists(User, n => n == 6))
                            {
                                answer = 9;
                            }
                            else
                            if (Array.Exists(User, n => n == 9))
                            {
                                answer = 6;
                            }
                            else
                            {
                                if (Array.Exists(User, n => n == 4))
                                {
                                    answer = 2;
                                }
                                else
                                {
                                    answer = 6;
                                }
                            }
                        } else
                        if (Array.Exists(User, n => n == 7))
                        {
                            if (Array.Exists(User, n => n == 4))
                            {
                                answer = 1;
                            }
                            else
                            if (Array.Exists(User, n => n == 1))
                            {
                                answer = 4;
                            }
                            else
                            if (Array.Exists(User, n => n == 8))
                            {
                                answer = 9;
                            }
                            else
                            if (Array.Exists(User, n => n == 9))
                            {
                                answer = 8;
                            }
                            else
                            {
                                if (Array.Exists(User, n => n == 2))
                                {
                                    answer = 6;
                                }
                                else
                                {
                                    answer = 2;
                                }
                            }
                        } else
                        if (Array.Exists(User, n => n == 9))
                        {
                            if (Array.Exists(User, n => n == 3))
                            {
                                answer = 6;
                            }
                            else
                            if (Array.Exists(User, n => n == 6))
                            {
                                answer = 3;
                            }
                            else
                            if (Array.Exists(User, n => n == 8))
                            {
                                answer = 7;
                            }
                            else
                            if (Array.Exists(User, n => n == 7))
                            {
                                answer = 8;
                            }
                            else
                            {
                                if (Array.Exists(User, n => n == 4))
                                {
                                    answer = 2;
                                }
                                else
                                {
                                    answer = 4;
                                }
                            }
                        }
                        else
                        {
                            switch (User[0])
                            {
                                case 2:
                                    answer = 1;
                                    break;
                                case 6:
                                    answer = 3;
                                    break;
                                case 4:
                                    answer = 7;
                                    break;
                                case 8:
                                    answer = 9;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        if (Array.Exists(User, n => n == 1))
                        {
                            if (CPU[0] != 9)
                            {
                                answer = 9;
                            }
                            else
                            {
                                answer = 3;
                            }
                        }
                        else if (Array.Exists(User, n => n == 2))
                        {
                            answer = 8;
                        }
                        else if (Array.Exists(User, n => n == 3))
                        {
                            if (CPU[0] != 7)
                            {
                                answer = 7;
                            }
                            else
                            {
                                answer = 9;
                            }
                        }
                        else if (Array.Exists(User, n => n == 4))
                        {
                            answer = 6;
                        }
                        else if (Array.Exists(User, n => n == 6))
                        {
                            answer = 4;
                        }
                        else if (Array.Exists(User, n => n == 7))
                        {
                            if (CPU[0] != 3)
                            {
                                answer = 3;
                            }
                            else
                            {
                                answer = 1;
                            }
                        }
                        else if (Array.Exists(User, n => n == 8))
                        {
                            answer = 2;
                        }
                        else if (Array.Exists(User, n => n == 9))
                        {
                            if (CPU[0] != 1)
                            {
                                answer = 1;
                            }
                            else
                            {
                                answer = 7;
                            }
                        }
                    }
                    break;
                case 4:
                    if (Array.Exists(User, n => n == formarLinea(CPU, simboloCPU, 1)))
                    {
                        if (formarLinea(User, simboloCPU, 1) == 0)
                        {
                            // en caso de no haber bloqueado
                            switch (formarLinea(CPU, simboloCPU, 1))
                            {
                                case 2:
                                    if (Array.Exists(User, n => n == 4) || Array.Exists(User, n => n == 7))
                                    {
                                        answer = 9;
                                    }
                                    else
                                    {
                                        answer = 7;
                                    }
                                    break;
                                case 4:
                                    if (Array.Exists(User, n => n == 8) || Array.Exists(User, n => n == 9))
                                    {
                                        answer = 3;
                                    }
                                    else
                                    {
                                        answer = 9;
                                    }
                                    break;
                                case 6:
                                    if (Array.Exists(User, n => n == 1) || Array.Exists(User, n => n == 2))
                                    {
                                        answer = 7;
                                    }
                                    else
                                    {
                                        answer = 1;
                                    }
                                    break;
                                case 8:
                                    if (Array.Exists(User, n => n == 1) || Array.Exists(User, n => n == 4))
                                    {
                                        answer = 3;
                                    }
                                    else
                                    {
                                        answer = 1;
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            answer = formarLinea(User, simboloCPU, 1);
                        }
                    }
                    else
                    {
                        answer = formarLinea(CPU, simboloCPU, 1);
                    }
                    break;
                case 5:
                    cs = 0; //casilla seleccionada
                    for (int i = 1; i < 4; i++)
                    {
                        if (formarLinea(CPU, simboloCPU, i) != 0 && BuscarCasilla().Contains(formarLinea(CPU, simboloCPU, i)) && cs == 0)
                        {
                            cs = formarLinea(CPU, simboloCPU, i);
                        }
                    }
                    for (int i = 1; i < 4; i++)
                    {
                        if (formarLinea(User, simboloCPU, i) != 0 && BuscarCasilla().Contains(formarLinea(User, simboloCPU, i)) && cs == 0)
                        {
                            cs = formarLinea(User, simboloCPU, i);
                        }
                    }
                    if (cs == 0)
                    {
                        for (int i = 0; i < BuscarCasilla().Length; i++)
                        {
                            if (BuscarCasilla()[i] != 0)
                            {
                                cs = BuscarCasilla()[i]; break;
                            }
                        }
                    }
                    answer = cs;
                    break;
                case 6:
                    cs = 0; //casilla seleccionada
                    for (int i = 1; i < 5; i++)
                    {
                        if (formarLinea(CPU, simboloCPU, i) != 0 && BuscarCasilla().Contains(formarLinea(CPU, simboloCPU, i)) && cs == 0)
                        {
                            cs = formarLinea(CPU, simboloCPU, i);
                        }
                    }
                    for (int i = 1; i < 5; i++)
                    {
                        if (formarLinea(User, simboloCPU, i) != 0 && BuscarCasilla().Contains(formarLinea(User, simboloCPU, i)) && cs == 0)
                        {
                            cs = formarLinea(User, simboloCPU, i);
                        }
                    }
                    if (cs == 0)
                    {
                        for (int i = 0; i < BuscarCasilla().Length; i++)
                        {
                            if (BuscarCasilla()[i] != 0)
                            {
                                cs = BuscarCasilla()[i]; break;
                            }
                        }
                    }
                    answer = cs;
                    break;
                case 7:
                    cs = 0; //casilla seleccionada
                    for (int i = 1; i < 5; i++)
                    {
                        if (formarLinea(CPU, simboloCPU, i) != 0 && BuscarCasilla().Contains(formarLinea(CPU, simboloCPU, i)) && cs == 0)
                        {
                            cs = formarLinea(CPU, simboloCPU, i);
                        }
                    }
                    for (int i = 1; i < 5; i++)
                    {
                        if (formarLinea(User, simboloCPU, i) != 0 && BuscarCasilla().Contains(formarLinea(User, simboloCPU, i)) && cs == 0)
                        {
                            cs = formarLinea(User, simboloCPU, i);
                        }
                    }
                    if (cs == 0)
                    {
                        for (int i = 0; i < BuscarCasilla().Length; i++)
                        {
                            if (BuscarCasilla()[i] != 0)
                            {
                                cs = BuscarCasilla()[i]; break;
                            }
                        }
                    }
                    answer = cs;
                    break;
                case 8:
                    int ultima = 0;
                    for (int i = 0; i < BuscarCasilla().Length; i++)
                    {
                        if (BuscarCasilla()[i] != 0)
                        {
                            ultima = BuscarCasilla()[i];
                            answer = ultima;
                            break;
                        }
                    }
                    break;
            }

            return answer;
        }

        int getCasilla(string btnName)
        {
            switch (btnName)
            {
                case "uno":
                    return 1;
                case "dos":
                    return 2;
                case "tres":
                    return 3;
                case "cuatro":
                    return 4;
                case "cinco":
                    return 5;
                case "seis":
                    return 6;
                case "siete":
                    return 7;
                case "ocho":
                    return 8;
                case "nueve":
                    return 9;
                default:
                    return 0;
            }
        }

        void marcarCasilla(int c, string simbolo)
        {
            switch (c)
            {
                case 1:
                    uno.Text = simbolo;
                    uno.Enabled = false;
                    break;
                case 2:
                    dos.Text = simbolo;
                    dos.Enabled = false;
                    break;
                case 3:
                    tres.Text = simbolo;
                    tres.Enabled = false;
                    break;
                case 4:
                    cuatro.Text = simbolo;
                    cuatro.Enabled = false;
                    break;
                case 5:
                    cinco.Text = simbolo;
                    cinco.Enabled = false;
                    break;
                case 6:
                    seis.Text = simbolo;
                    seis.Enabled = false;
                    break;
                case 7:
                    siete.Text = simbolo;
                    siete.Enabled = false;
                    break;
                case 8:
                    ocho.Text = simbolo;
                    ocho.Enabled = false;
                    break;
                case 9:
                    nueve.Text = simbolo;
                    nueve.Enabled = false;
                    break;
            }
        }
        
        private void btn_JvsJ_Click(object sender, EventArgs e)
        {
            caso = 2;
            IniciarJuego();
            BuscarCasilla();
        }

        private void btn_JvsC_Click(object sender, EventArgs e)
        {
            caso = 1;
            IniciarJuego();
            BuscarCasilla();
        }

        private void btn_CvsC_Click(object sender, EventArgs e)
        {
            equis = "CPU 1";
            circulo = "CPU 2";
            radioJugador1.Text = circulo;
            radioJugador2.Text = equis;
            label2.Visible = true;
            gameFinished = false;
            DibujarTablero();
            Boolean bandera = cpuFirst.Checked;
            while (!gameFinished)
            {
                if (bandera)
                {
                    radioJugador2.Checked = true;
                    ganarAprender("X", true, 0);
                    GanarPartida();
                    bandera = false;
                }
                else
                {
                    radioJugador1.Checked = true;
                    int cs = ganarEstrategia("O");
                    ganarAprender("O", false, cs);
                    marcarCasilla(cs, "O");
                    GanarPartida();
                    bandera = true;
                }
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
